from flask import Flask
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)


class Ping(Resource):
    def get(self):
        return {'API': 'is working'}


api.add_resource(Ping, '/v1/')
