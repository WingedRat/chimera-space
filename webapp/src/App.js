import React, {Component} from 'react';

import './App.scss';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {library} from '@fortawesome/fontawesome-svg-core'

import {faEnvelope} from '@fortawesome/free-solid-svg-icons'
import {faQuestion} from '@fortawesome/free-solid-svg-icons'
import {faCopyright} from '@fortawesome/free-regular-svg-icons'
import {faMobileAlt} from '@fortawesome/free-solid-svg-icons'
import {faClipboard} from '@fortawesome/free-regular-svg-icons'
import {faIdCard} from '@fortawesome/free-regular-svg-icons'

import {faKeybase} from '@fortawesome/free-brands-svg-icons'
import {faTelegramPlane} from '@fortawesome/free-brands-svg-icons'
import {faDiscord} from '@fortawesome/free-brands-svg-icons'
import {faMastodon} from '@fortawesome/free-brands-svg-icons'
import {faTwitter} from '@fortawesome/free-brands-svg-icons'
import {faGooglePlusG} from '@fortawesome/free-brands-svg-icons'
import {faSkype} from '@fortawesome/free-brands-svg-icons'
import {faWhatsapp} from '@fortawesome/free-brands-svg-icons'
import {faGithub} from '@fortawesome/free-brands-svg-icons'
import {faStackOverflow} from '@fortawesome/free-brands-svg-icons'
import {faLinkedinIn} from '@fortawesome/free-brands-svg-icons'
import {faLastfm} from '@fortawesome/free-brands-svg-icons'
import {faSteamSymbol} from '@fortawesome/free-brands-svg-icons'

import {faWindows} from '@fortawesome/free-brands-svg-icons'
import {faLinux} from '@fortawesome/free-brands-svg-icons'

import {Button, ButtonGroup, Card, Col, Navbar, NavbarBrand, Row, Tooltip, Table} from 'reactstrap';
import axios from 'axios';
import {CopyToClipboard} from 'react-copy-to-clipboard';


library.add(faEnvelope,
    faQuestion,
    faCopyright,
    faKeybase,
    faTelegramPlane,
    faDiscord,
    faMastodon,
    faTwitter,
    faGooglePlusG,
    faSkype,
    faWhatsapp,
    faGithub,
    faStackOverflow,
    faLinkedinIn,
    faLastfm,
    faSteamSymbol,
    faMobileAlt,
    faWindows,
    faLinux,
    faClipboard,
    faIdCard);

const uuidv1 = require('uuid/v1');

class App extends Component {
    render() {
        return (
            <div>
                <Navbar className="cs-navbar" dark expand="md" fixed="top">
                    <NavbarBrand href="/">Chimera Space</NavbarBrand>
                </Navbar>
                <Main/>
                <Footer/>
            </div>
        )
    }
}

class Footer extends Component {
    render() {
        return (
            <footer>
                <div><FontAwesomeIcon icon={faCopyright} rotation={180}/>
                    <a className="ml-1 mr-2" href="https://keybase.io/wingedrat">WingedRat</a>
                    2018
                </div>
            </footer>
        )
    }
}

class Main extends Component {
    render() {
        return (
            <main role="main" className="container-flex">
                <Card className="text-center cs-maincard">
                    <Row className="row-eq-height">
                        <Col lg="3">
                            <div className="container h-100">
                                <img className="img-fluid cs-avatar border border-dark"
                                     src="img/avatar.jpg" alt="Avatar"/>
                            </div>
                        </Col>
                        <Col lg="9">
                            <div className="card-body cs-card h-100">
                                <h5 className="card-title">Andrey Yudin</h5>
                                <div className="card-text">
                                    <Table>
                                        <tbody>
                                        <tr>
                                            <td>Age</td>
                                            <td>23</td>
                                        </tr>
                                        <tr>
                                            <td>Languages</td>
                                            <td>russian (native speaker), english (~B2), german (~A2)</td>
                                        </tr>
                                        <tr>
                                            <td>Location</td>
                                            <td>Saint Petersburg, Russian Federation</td>
                                        </tr>
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <Row className="mt-4">
                        <Col lg="3">
                            <KeysCard/>
                        </Col>
                        <Col lg="9">
                            <Card className="card-body cs-card" id="contacts-card">
                                <div className="container-fluid">
                                    <Row className="justify-content-center">
                                        <ContactCard name="Keybase" link="https://keybase.io/wingedrat"
                                                     icon={faKeybase}/>
                                        <ContactCard name="Telegram" link="https://t.me/wingedrat"
                                                     icon={faTelegramPlane}/>
                                        <ContactCard name="Discord" link="https://discord.gg/WvFPNpE"
                                                     icon={faDiscord}/>
                                        <ContactCard name="Mail" link="mailto:chimera@chimera.website"
                                                     icon={faEnvelope}/>
                                        <ContactCard name="Mastodon" link="https://mastodon.chimera.website/@chimera"
                                                     icon={faMastodon} tooltip="Can be broken right now"/>
                                        <ContactCard name="Twitter" link="https://twitter.com/Sleepy_Chimera"
                                                     icon={faTwitter}/>
                                        <ContactCard name="Google+"
                                                     link="https://plus.google.com/u/0/104338403412861071253"
                                                     icon={faGooglePlusG}/>
                                        <ContactCard name="Skype" link="skype:andrey_168?chat" icon={faSkype}/>
                                        <ContactCard name="WhatsApp"
                                                     link="https://api.whatsapp.com/send?phone=79217883845"
                                                     icon={faWhatsapp}/>
                                        <ContactCard name={"GitHub"} link={"https://github.com/WingedRat"}
                                                     icon={faGithub}/>
                                        <ContactCard name={"Stack"}
                                                     link={"https://stackoverflow.com/users/4614217/andrey-yudin"}
                                                     icon={faStackOverflow}/>
                                        <ContactCard name={"LinkedIn"}
                                                     link={"https://www.linkedin.com/in/%D0%B0%D0%BD%D0%B4%D1%80%D0%B5%D0%B9-%D1%8E%D0%B4%D0%B8%D0%BD-851a77102"}
                                                     icon={faLinkedinIn}/>
                                        <ContactCard name={"Last.fm"} link={"https://www.last.fm/user/Winged_Rat"}
                                                     icon={faLastfm}/>
                                        <ContactCard name={"CuriousCat"} link={"https://curiouscat.me/Comrade_Chimera"}
                                                     icon={faQuestion}/>
                                        <ContactCard name={"Steam"} link={"https://steamcommunity.com/id/RayChimera/"}
                                                     icon={faSteamSymbol}/>
                                    </Row>
                                </div>
                            </Card>
                        </Col>
                    </Row>
                </Card>
            </main>
        );
    }
}

class ContactCard extends React.Component {
    constructor(props) {
        super(props);

        this.uuid = uuidv1();
        this.toggle = this.toggle.bind(this);
        this.state = {
            tooltipOpen: false
        };
    }

    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }

    render() {
        if (this.props.tooltip) {
            var tooltip_content = (
                <Tooltip placement="bottom" isOpen={this.state.tooltipOpen}
                         target={'ContactCard-Button_' + this.uuid} toggle={this.toggle}>
                    {this.props.tooltip}
                </Tooltip>
            )
        }
        return (
            <Card className={"cs-card cs-contact-card m-2"}>
                <div className="card-body">
                    <p className="card-text"><FontAwesomeIcon icon={this.props.icon} size="3x"/></p>
                    <Button id={'ContactCard-Button_' + this.uuid} color="dark" size="sm" href={this.props.link}
                            target="_blank" rel="noopener noreferrer me" className="text-center">
                        {this.props.name}
                    </Button>
                    {tooltip_content}
                </div>
            </Card>
        )
    }
}

class KeysCard extends Component {
    constructor(props) {
        super(props);

        this.state = {key_text: "^ Press any button above ^"};
    };

    componentDidMount() {
        axios.get(`keys.json`)
            .then(res => {
                const keys = res.data;
                this.setState({keys});
            })
    }

    setKeyText(key) {
        this.setState({key_text: this.state.keys[key]});
    };

    render() {
        return (
            <div>
                <ButtonGroup className={"cs-keys-card-button-group"} size="sm">
                    <Button className={"cs-keys-card-button"} onClick={() => this.setKeyText("gpg")} color="dark">
                        <FontAwesomeIcon icon={faIdCard}/> GPG
                    </Button>
                    <Button className={"cs-keys-card-button"} onClick={() => this.setKeyText("windows-ssh")} color="dark" type="button">
                        <FontAwesomeIcon icon={faWindows}/> SSH
                    </Button>
                    <Button className={"cs-keys-card-button"} onClick={() => this.setKeyText("linux-ssh")} color="dark" type="button">
                        <FontAwesomeIcon icon={faLinux}/> SSH
                    </Button>
                    <Button className={"cs-keys-card-button"} onClick={() => this.setKeyText("mobile-ssh")} color="dark" type="button">
                        <FontAwesomeIcon icon={faMobileAlt}/> SSH
                    </Button>
                    <CopyToClipboard text={this.state.key_text}>
                        <Button className={"cs-keys-card-button"} color="dark" type="button">
                            <FontAwesomeIcon icon={faClipboard}/> Copy
                        </Button>
                    </CopyToClipboard>
                </ButtonGroup>
                <Card className="cs-keys-block cs-card h-100 mt-3">
                    <div id="keys-div" className="cs-keys-card">{this.state.key_text}</div>
                </Card>
            </div>
        )
    }

}

export default App;
